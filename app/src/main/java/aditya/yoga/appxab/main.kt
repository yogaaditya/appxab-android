package aditya.yoga.appxab

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.main_menu.*

class main : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_menu)
        mhs.setOnClickListener(this)
        prodi.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.mhs->{
                val pindah =  Intent(this,MainActivity::class.java)
                startActivity(pindah)
            }
            R.id.prodi->{
                val pindah = Intent(this,kategori::class.java)
                startActivity(pindah)

            }
        }
    }
}